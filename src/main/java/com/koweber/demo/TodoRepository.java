package com.koweber.demo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<Todo, Integer> {

  List<Todo> findByDone(boolean done);

  Todo findById(int id);

}