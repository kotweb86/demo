package com.koweber.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Some details")
@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int id;
    public String item;
    @ApiModelProperty(notes = "Is task already done?")
    public boolean done;
}
