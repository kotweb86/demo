package com.koweber.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/todo")
public class TodoController {

    @Autowired
    TodoRepository todoRepository;

    @GetMapping("/")
    @ApiOperation(value = "Gets list of all tosos")
    public List<Todo> getTodos() {
        List<Todo> returnList = new ArrayList<Todo>();
        todoRepository.findAll().forEach(returnList::add);
        return returnList;
    }

    @GetMapping("/{id}")
    public Todo getTodo(@ApiParam(value = "is number of Todo", required = true) @PathVariable String id) {
        return todoRepository.findById(Integer.parseInt(id));
    }

    @PostMapping("/")
    public Todo addTodo(@RequestParam String item) {
        Todo t = new Todo();
        t.setItem(item);
        t.setDone(false);
        todoRepository.save(t);
        return t;
    }

}
